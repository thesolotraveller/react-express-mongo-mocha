// importing models
const Case = require("./models/Case");
const Officer = require("./models/Officer");

// importing services
const Validator = require("./services/ValidatorService");

const createOfficer = (req, res) => {

  const schema = Validator.Schema.object().keys({
    name: Validator.Schema.string().required().label('Officer Name')
  });

  let body;

  Validator.validate(req.body, schema)
    .then(validatedReqBody => {
      body = validatedReqBody;

      return Officer.find(body);
    })
    .then(officers => {
      if (officers.length >= 1) {
        throw new Error("SimilarResourceExist");
      } else {
        const officer = new Officer(body);
        return officer.save(body);
      }
    })
    .then(response => {
      if (!response) throw new Error('ServerSideError');
      res.status(201).json(response);
    })
    .catch(err => {
      console.log(err);

      if (err.name === 'ValidationError') {
        return res.status(400).json({
          message: 'Validation error',
          errors: err.details
        });
      } else if (err.message === 'SimilarResourceExist') {
        return res.status(400).json({
          message: 'Similar record already exists in database',
          errors: err.details
        });
      }

      return res.status(500).json({
        error: {
          message: "Sorry, there was an error in serving you the requested resource",
          code: err.code
        }
      });
    })
}

const createCase = (req, res) => {

  const detailsSchema = Validator.Schema.object().keys({
    entity: Validator.Schema.string().valid('vehicle').required().label('Entity type'),
    id: Validator.Schema.string().required().label('Unique id for the entity')
  });

  const schema = Validator.Schema.object().keys({
    type: Validator.Schema.string().valid('stolen').required().label('Case type'),
    details: detailsSchema
  });

  let body, caseCreateResponse;

  Validator.validate(req.body, schema)
    .then(validatedReqBody => {
      body = validatedReqBody;
      body.resolved = false;

      return Case.find(body);
    })
    .then(cases => {
      if (cases.length >= 1) {
        throw new Error("SimilarResourceExist");
      } else {
        return Officer.findOne({ free: true });
      }
    })
    .then(officer => {
      if (officer && officer._id) {
        body.officer = officer._id;
        body.assigned = true;
      }

      const newCase = new Case(body);
      return newCase.save(body);
    })
    .then(response => {
      if (!response) throw new Error('ServerSideError');
      caseCreateResponse = response;

      // making the assined officer officer unavailable for further assignments
      return Officer.updateOne({_id: body.officer}, {free: false});
    })
    .then(() => {
      res.status(201).json(caseCreateResponse);
    })
    .catch(err => {
      console.log(err);

      if (err.name === 'ValidationError') {
        return res.status(400).json({
          message: 'Validation error',
          errors: err.details
        });
      } else if (err.message === 'SimilarResourceExist') {
        return res.status(409).json({
          message: 'Similar record already exists in database',
          errors: err.details
        });
      }

      return res.status(500).json({
        error: {
          message: "Sorry, there was an error in serving you the requested resource",
          code: err.code
        }
      });
    })
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * 
 * at the moment, this handler is capable of taking in
 * just the resolved status of the case and taking actions accordingly
 */
const resolveCase = (req, res) => {
  const caseId = req.params.id;
  let officerId;

  return Case.findOne( {_id: caseId})
    .then(foundCase => {
      if (!foundCase) throw new Error('ResourceNotFound');
      else if (foundCase && foundCase.resolved) throw new Error('CaseResolvedAlready');
      officerId = foundCase.officer;

      return Case.updateOne({_id: caseId}, {resolved: true});
    })
    .then(() => {
      return Officer.updateOne({_id: officerId}, {free: true});
    })
    .then(() => {
      return Case.findOne({assigned: false});
    })
    .then(foundCase => {
      if (foundCase && foundCase._id) {
        return Case.updateOne({_id: foundCase.id}, {
          officer: officerId,
          assigned: true
        });
      }

      return null;
    })
    .then(() => {
      res.status(200).json({
        message: 'Case resolved, officer relieved from case and might have been reassigned'
      });
    })
    .catch(err => {
      console.log(err);

      if (err.message === 'ResourceNotFound') {
        return res.status(400).json({
          message: 'Case not found',
          errors: err.details
        });
      } else if (err.message === 'CaseResolvedAlready') {
        return res.status(409).json({
          message: 'Case already resolved',
          errors: err.details
        });
      }

      return res.status(500).json({
        error: {
          message: "Sorry, there was an error in serving you the requested resource",
          code: err.code
        }
      });
    })
}

/**
 * this endpoint is for listing the cases in paginated way
 * default is supplying 10 cases per page
 */
const listCases = (req, res) => {
  const page = !isNaN(req.query.page) ? parseInt(req.query.page) : 1,
    limit = !isNaN(req.query.limit) ? parseInt(req.query.limit) : 10;

  Case.find()
    .skip((page-1)*limit)
    .limit(limit)
    .populate('officer')
    .then(cases => {
      if (!cases) throw new Error('ServerSideError');

      res.status(200).json(cases);
    })
    .catch(err => {
      console.log(err);

      return res.status(500).json({
        error: {
          message: "ServerSideError",
          code: err.details
        }
      });
    })
}

const countCases = (req, res) => {
  Case.countDocuments()
    .then(count => res.status(200).json(count))
    .catch(err => {
      sails.log.debug(err);

      return res.status(500).json({
        code: 'ServerSideError',
        message: err.detail
      });
    })
} 

module.exports = {
  createOfficer: createOfficer,
  createCase: createCase,
  resolveCase: resolveCase,
  listCases: listCases,
  countCases: countCases
}