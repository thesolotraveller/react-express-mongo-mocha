const mongoose = require('mongoose');

const CaseSchema = new mongoose.Schema({
  type: { type: String, required: true },
  details: {
    entity: { type: String, required: true },
    id: { type: String, required: true }
  },
  assigned: { type: Boolean, required: true, default: false },
  resolved: { type: Boolean, required: true, default: false },
  officer: {type: mongoose.Schema.Types.ObjectId, ref: 'Officer' }
}, { versionKey: false });

module.exports = mongoose.model('Case', CaseSchema);