const mongoose = require('mongoose');

const OfficerSchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  case: {type: mongoose.Schema.Types.ObjectId, ref: 'Case'},
  free: { type: Boolean, required: true, default: true }
}, { versionKey: false });

module.exports = mongoose.model('Officer', OfficerSchema);