const apiServer = require('./server');
const db = require('./db/index');

const app = apiServer.server();
const port = process.env.PORT || '3600';
app.set('port', port);

db.connect()
  .then(() => {
    console.log('\nSuccessfully connected to database');
    app.listen(port, () => {
      console.log(`\nAPI server running on PORT: ${port}\n`);
    });
  })