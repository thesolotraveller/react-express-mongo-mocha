const Promise = require('bluebird');
const Joi = require('@hapi/joi');

module.exports = {

  Schema: Joi,

  validate: function (body, schema) {

    return new Promise ((resolve, reject) => {
      Joi.validate(body, schema, (err, value) => {
        if (err) return reject(err);
        return resolve(value)
      });
    })
  }
};