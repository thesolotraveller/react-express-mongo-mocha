// requiring module to read credentials from .env
require('dotenv').config();
const mongoose = require('mongoose');

const host = process.env.DB_HOST ? process.env.DB_HOST : 'localhost';
const dbName = process.env.NODE_ENV === 'testing' ? process.env.DB_NAME + '_test' : process.env.DB_NAME;

// mongoDb connection string
const mongo_uri = `mongodb://${host}/${dbName}`;

const connect = () => {
  return new Promise((resolve, reject) => {
    const mongoOpts = { useNewUrlParser: true, useCreateIndex: true };
    mongoose.connect(mongo_uri, mongoOpts, function(err) {
      if (err) return reject();
      return resolve();
    });   
  })
}

const close = () => {
  return mongoose.disconnect();
}

module.exports = { connect, close }