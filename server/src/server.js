// importing npm dependecies
const express    = require('express');
const bodyParser = require('body-parser');
const cors       = require('cors');

const handlers = require('./handler');

const server = () => {

	// declaring a new express app
	const app = express();
	
	// adding middlewares to the express app declared above
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());
	app.use(cors());

	// officer model routes
  app.post('/officer', handlers.createOfficer);
  
  // case model routes
  app.post('/case', handlers.createCase);
  app.post('/case/:id/resolve', handlers.resolveCase);
  app.get('/cases', handlers.listCases);
  app.get('/countCases', handlers.countCases);

	return app;
}

module.exports = { server }