const expect = require('chai').expect;
const request = require('supertest');

const server = require('../../src/server').server();
const db = require('../../src/db/index');

describe('POST /officer', () => {
  
  before(done => {
    db.connect()
      .then(() => done())
      .catch(e => done(e))
  })

  it('creating a new police officer with correct request body', (done) => {
    request(server).post('/officer')
      .send({name: 'John Doe'})
      .then(res => {
        const body = res.body;
        expect(body).to.contain.property('_id');
        expect(body).to.contain.property('name');
        expect(body).to.contain.property('free');
        done();
      });
  })

  it('creating a new police officer with incorrect request body by supplying an extra param like age', (done) => {
    request(server).post('/officer')
      .send({name: 'John Doe', age: 25})
      .then(res => {
        expect(res).to.contain.property('status');
        expect(res.status).to.equal(400);
        done();
      });
  })

  it('creating two new police officers with same name should not be allowed', (done) => {
    request(server).post('/officer')
      .send({name: 'Will Smith'})
      .then(() => {
        return request(server)
        .post('/officer')
        .send({name: 'Will Smith'})
      })
      .then(res => {
        expect(res).to.contain.property('status');
        expect(res.status).to.equal(400);
        done();
      });
  })

  after(done => {
    db.close()
      .then(() => done())
      .catch(e => done(e))
  })
})