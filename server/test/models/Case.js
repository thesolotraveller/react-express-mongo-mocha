const expect = require('chai').expect;
const request = require('supertest');

const server = require('../../src/server').server();
const db = require('../../src/db/index');

describe('POST /case', () => {  
  before(done => {
    db.connect()
      .then(() => done())
      .catch(e => done(e))
  })

  it('creating a new case with incorrect request body by supplying an invalid case type', (done) => {
    request(server).post('/case')
      .send({type: 'robbery', details: { entity: 'vehicle', id: 'TS09 SC 6537'}})
      .then(res => {
        expect(res).to.contain.property('status');
        expect(res.status).to.equal(400);
        done();
      });
  })

  it('creating a new case with incorrect request body by supplying an invalid entity in details', (done) => {
    request(server).post('/case')
      .send({type: 'stolen', details: { entity: 'mobile', id: '446462626492629494'}})
      .then(res => {
        expect(res).to.contain.property('status');
        expect(res.status).to.equal(400);
        done();
      });
  })

  after(done => {
    db.close()
      .then(() => done())
      .catch(e => done(e))
  })
})