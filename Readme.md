Please ensure that `MongoDB` and `Node` are installed.
Then follow below steps in order.

Navigate to ~/server
    - execute `npm install`
    - execute `npm test`
    - excute `npm start`

Navigate to ~/client
    - execute `npm install`
    - execute `npm start`

Frontend webapp will start on http://localhost:3000

Please find attached screenshots of how it looks in actions