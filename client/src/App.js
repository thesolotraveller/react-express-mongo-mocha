import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Table } from 'reactstrap'
import axios from 'axios';

import hostUrl from './config';
import 'bootstrap/dist/css/bootstrap.css';

class Main extends Component {
  constructor(props){
    super(props);
    this.state = {
      carRegNum: '',
      caseList: [],
      page: 1,
      limit: 10,
      buttonCount: 0,
      showLoader: false
    }
  }

  componentDidMount() {
    axios.get(`${hostUrl}cases?page=${this.state.page}&limit=${this.state.limit}`)
      .then(cases => {
        if (!(cases && cases.data)) this.setState({showLoader: true});
        this.setState({caseList: cases.data});

        return axios.get(`${hostUrl}countCases`);
      })
      .then (count => {
        this.setState({buttonCount: (count.data + (10 - count.data%10))/10});
      })
      .catch(err => {
        console.log(err);
        this.setState({showLoader: true});
      })
  }

  handleRegNumChange(e){ this.setState( { carRegNum: e.target.value }) }

  getPageResult(e) {
    let page = e.target.getAttribute("data-index");
    this.setState({page});
    axios.get(`${hostUrl}cases?page=${page}&limit=${this.state.limit}`)
      .then (cases => {
        this.setState({ caseList: cases.data });
      })
      .catch(error => {
        this.setState({error: error.message})
      })
  }

  reportStolenCar (e) {
    e.preventDefault();
    let newCaseBody = {
      type: 'stolen',
      details: {
        entity: 'vehicle',
        id: this.state.carRegNum
      }
    }

    axios.post(hostUrl + 'case', newCaseBody)
      .then(() => {
        if (this.state.caseList.length > 9) {
          this.setState(prevState => ({
            page: prevState.page + 1,
            buttonCount: prevState.buttonCount + 1
          }))
        }

        return axios.get(`${hostUrl}cases?page=${this.state.page}&limit=${this.state.limit}`);
      })
      .then (cases => {
        this.setState({ caseList: cases.data });
      })
      .catch(error => {
        this.setState({error: error.message})
      })
      .catch(error => {
        console.log(error);
      });
  }

  resolve(caseId) {
    axios.post(`${hostUrl}case/${caseId}/resolve`, {})
      .then(response => {
        if (response.status === 200)
          return axios.get(`${hostUrl}cases?page=${this.state.page}&limit=${this.state.limit}`);
        else
          return null;
      })
      .then (cases => {
        this.setState({ caseList: cases.data });
      })
      .catch(error => {
        this.setState({error: error.message})
      })
  }

  render() {

    const mainDiv = {
      maxWidth: '60%',
      marginLeft: '20%',
      marginTop: '50px'
    }, tableStyle = {
      marginTop: '100px'
    };

    const allCases = this.state.caseList.map((eachCase, i) => {
      return <tr key={eachCase._id}>
        <th>{i+1}</th>
        <th>{eachCase.details.id}</th>
        <th>{eachCase.assigned ? 'Yes' : 'No'}</th>
        <th>{eachCase.officer ? eachCase.officer.name : 'Not Assigned Yet'}</th>
        <th>{eachCase.resolved ? 'Yes' : 'No'}</th>
        <th>{(!eachCase.resolved && eachCase.assigned) ? <Button color="primary" onClick={this.resolve.bind(this, eachCase._id)}>Resolve</Button> : '-----'}</th>
      </tr>;
    })

    const paginationButtons = [];
    for (let i=0; i<this.state.buttonCount; ++i) {
      paginationButtons.push(<Button color="primary" data-index={i+1} key={i+1} onClick={this.getPageResult.bind(this)}>{i+1}</Button>);
    }

    return (
      <div style={mainDiv}>
        <Form>
          <FormGroup>
            <Label for="exampleText">Registration number of your stolen car?</Label>
            <Input type="text" name="text" id="exampleText" onChange={this.handleRegNumChange.bind(this)}/>
          </FormGroup>
          <Button onClick={this.reportStolenCar.bind(this)}>Add</Button>
        </Form>

        <Table style={tableStyle}>
          <thead>
            <tr>
              <th>#</th>
              <th>Car Reg. No.</th>
              <th>Officer Assigned</th>
              <th>Officer Name</th>
              <th>Case resolved</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {allCases}
          </tbody>
        </Table>

        <div>
          {paginationButtons}
        </div>
      </div>
    );
  }
}

export default Main;
